<?php

namespace App\Entity\Produits;

use App\Repository\Produits\TarifRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TarifRepository::class)]
class Tarif
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $libelle = null;

    #[ORM\Column]
    private ?float $prixDemi = null;

    #[ORM\Column]
    private ?float $prixPinte = null;

    #[ORM\Column(nullable: true)]
    private ?float $prixPichet = null;

    #[ORM\OneToMany(mappedBy: 'tarif', targetEntity: Produit::class)]
    private Collection $produits;

    public function __construct()
    {
        $this->produits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPrixDemi(): ?float
    {
        return $this->prixDemi;
    }

    public function setPrixDemi(float $prixDemi): self
    {
        $this->prixDemi = $prixDemi;

        return $this;
    }

    public function getPrixPinte(): ?float
    {
        return $this->prixPinte;
    }

    public function setPrixPinte(float $prixPinte): self
    {
        $this->prixPinte = $prixPinte;

        return $this;
    }

    public function getPrixPichet(): ?float
    {
        return $this->prixPichet;
    }

    public function setPrixPichet(?float $prixPichet): self
    {
        $this->prixPichet = $prixPichet;

        return $this;
    }

    /**
     * @return Collection<int, Produit>
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Produit $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits->add($produit);
            $produit->setTarif($this);
        }

        return $this;
    }

    public function removeProduit(Produit $produit): self
    {
        if ($this->produits->removeElement($produit)) {
            // set the owning side to null (unless already changed)
            if ($produit->getTarif() === $this) {
                $produit->setTarif(null);
            }
        }

        return $this;
    }
}
