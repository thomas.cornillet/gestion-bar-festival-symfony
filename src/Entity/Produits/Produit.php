<?php

namespace App\Entity\Produits;

use App\Repository\Produits\ProduitRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProduitRepository::class)]
class Produit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    #[ORM\Column]
    private ?float $stock = null;

    #[ORM\Column(nullable: true)]
    private ?float $alcool = null;

    #[ORM\Column(nullable: true)]
    private ?bool $gaz = null;

    #[ORM\Column]
    private ?bool $pichet = null;

    #[ORM\ManyToOne(inversedBy: 'produits')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tarif $tarif = null;

    #[ORM\ManyToOne(inversedBy: 'produits')]
    private ?Style $style = null;

    #[ORM\ManyToOne(inversedBy: 'produits')]
    private ?Type $type = null;

    #[ORM\ManyToOne(inversedBy: 'produits')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Categorie $categorie = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getStock(): ?float
    {
        return $this->stock;
    }

    public function setStock(float $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getAlcool(): ?float
    {
        return $this->alcool;
    }

    public function setAlcool(?float $alcool): self
    {
        $this->alcool = $alcool;

        return $this;
    }

    public function isGaz(): ?bool
    {
        return $this->gaz;
    }

    public function setGaz(?bool $gaz): self
    {
        $this->gaz = $gaz;

        return $this;
    }

    public function isPichet(): ?bool
    {
        return $this->pichet;
    }

    public function setPichet(bool $pichet): self
    {
        $this->pichet = $pichet;

        return $this;
    }

    public function getTarif(): ?Tarif
    {
        return $this->tarif;
    }

    public function setTarif(?Tarif $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getStyle(): ?Style
    {
        return $this->style;
    }

    public function setStyle(?Style $style): self
    {
        $this->style = $style;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }
}
