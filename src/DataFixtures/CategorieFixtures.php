<?php

namespace App\DataFixtures;

use App\Entity\Produits\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CategorieFixtures extends Fixture implements FixtureGroupInterface, OrderedFixtureInterface
{
    public const BEER_CATEGORIE_REFERENCE = 'beer-categorie';
    public const SOFT_CATEGORIE_REFERENCE = 'soft-categorie';

    public function load(ObjectManager $manager): void
    {
        $beer = (new Categorie())->setLibelle('Beer');
        $manager->persist($beer);

        $soft = (new Categorie())->setLibelle('Soft');
        $manager->persist($soft);

        $manager->flush();

        $this->addReference(self::BEER_CATEGORIE_REFERENCE, $beer);
        $this->addReference(self::SOFT_CATEGORIE_REFERENCE, $soft);
    }

    public static function getGroups(): array
    {
        return ['categorie'];
    }

    public function getOrder(): int
    {
        return 1;
    }
}
