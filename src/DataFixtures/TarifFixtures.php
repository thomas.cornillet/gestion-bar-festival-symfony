<?php

namespace App\DataFixtures;

use App\Entity\Produits\Tarif;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TarifFixtures extends Fixture implements FixtureGroupInterface, OrderedFixtureInterface
{
    public const SODA_TARIF_REFERENCE = 'soda-tarif';
    public const EAU_TARIF_REFERENCE = 'eau-tarif';
    public const BIERE_BASE_TARIF_REFERENCE = 'biere-base-tarif';
    public const CRAFT_BEER_TARIF_REFERENCE = 'craft-beer-tarif';
    public const CRAFT_SOFT_TARIF_REFERENCE = 'craft-soft-tarif';

    public function load(ObjectManager $manager): void
    {
        $soda = (new Tarif())
            ->setLibelle('soda')
            ->setPrixDemi(2)
            ->setPrixPinte(3)
            ->setPrixPichet(8);
        $manager->persist($soda);

        $eau = (new Tarif())
            ->setLibelle('eau')
            ->setPrixDemi(1)
            ->setPrixPinte(1,5)
            ->setPrixPichet(5);
        $manager->persist($eau);

        $biereBase = (new Tarif())
            ->setLibelle('base beer')
            ->setPrixDemi(2.5)
            ->setPrixPinte(4)
            ->setPrixPichet(12);
        $manager->persist($biereBase);

        $craftBeer = (new Tarif())
            ->setLibelle('craft beer')
            ->setPrixDemi(3.5)
            ->setPrixPinte(7);
        $manager->persist($craftBeer);

        $craftSoft = (new Tarif())
            ->setLibelle('craft soft')
            ->setPrixDemi(3)
            ->setPrixPinte(5);
        $manager->persist($craftSoft);

        $manager->flush();

        $this->addReference(self::SODA_TARIF_REFERENCE, $soda);
        $this->addReference(self::EAU_TARIF_REFERENCE, $eau);
        $this->addReference(self::BIERE_BASE_TARIF_REFERENCE, $biereBase);
        $this->addReference(self::CRAFT_BEER_TARIF_REFERENCE, $craftBeer);
        $this->addReference(self::CRAFT_SOFT_TARIF_REFERENCE, $craftSoft);
    }

    public static function getGroups(): array
    {
        return ['style'];
    }

    public function getOrder(): int
    {
        return 3;
    }
}
