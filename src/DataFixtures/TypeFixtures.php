<?php

namespace App\DataFixtures;

use App\Entity\Produits\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TypeFixtures extends Fixture implements FixtureGroupInterface, OrderedFixtureInterface
{
    public const SODA_TYPE_REFERENCE = 'soda-type';
    public const ICETEA_TYPE_REFERENCE = 'icetea-type';
    public const EAU_TYPE_REFERENCE = 'eau-type';
    public const AUTRE_TYPE_REFERENCE = 'autre-type';

    public function load(ObjectManager $manager): void
    {
        $soda = (new Type())->setLibelle('soda');
        $manager->persist($soda);

        $iceTea = (new Type())->setLibelle('ice tea');
        $manager->persist($iceTea);

        $eau = (new Type())->setLibelle('eau');
        $manager->persist($eau);

        $autre = (new Type())->setLibelle('autre');
        $manager->persist($autre);

        $manager->flush();

        $this->addReference(self::SODA_TYPE_REFERENCE, $soda);
        $this->addReference(self::ICETEA_TYPE_REFERENCE, $iceTea);
        $this->addReference(self::EAU_TYPE_REFERENCE, $eau);
        $this->addReference(self::AUTRE_TYPE_REFERENCE, $autre);
    }

    public static function getGroups(): array
    {
        return ['type'];
    }

    public function getOrder(): int
    {
        return 4;
    }
}
