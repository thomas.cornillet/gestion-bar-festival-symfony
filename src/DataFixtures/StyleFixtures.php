<?php

namespace App\DataFixtures;

use App\Entity\Produits\Style;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StyleFixtures extends Fixture implements FixtureGroupInterface, OrderedFixtureInterface
{
    public const PILS_STYLE_REFERENCE = 'pils-style';
    public const IPA_STYLE_REFERENCE = 'ipa-style';
    public const NEIPA_STYLE_REFERENCE = 'neipa-style';
    public const WEIZEN_STYLE_REFERENCE = 'weizen-style';

    public function load(ObjectManager $manager): void
    {
        $pils = (new Style())->setLibelle('Pils');
        $manager->persist($pils);

        $ipa = (new Style())->setLibelle('IPA');
        $manager->persist($ipa);

        $neipa = (new Style())->setLibelle('NEIPA');
        $manager->persist($neipa);

        $weizen = (new Style())->setLibelle('Weizen');
        $manager->persist($weizen);

        $manager->flush();

        $this->addReference(self::PILS_STYLE_REFERENCE, $pils);
        $this->addReference(self::IPA_STYLE_REFERENCE, $ipa);
        $this->addReference(self::NEIPA_STYLE_REFERENCE, $neipa);
        $this->addReference(self::WEIZEN_STYLE_REFERENCE, $weizen);
    }

    public static function getGroups(): array
    {
        return ['style'];
    }

    public function getOrder(): int
    {
        return 2;
    }
}
