<?php

namespace App\DataFixtures;

use App\Entity\Produits\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProduitFixtures extends Fixture implements FixtureGroupInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $orangina = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::SOFT_CATEGORIE_REFERENCE))
            ->setNom('Orangina')
            ->setGaz(true)
            ->setType($this->getReference(TypeFixtures::SODA_TYPE_REFERENCE))
            ->setPichet(true)
            ->setStock(200)
            ->setTarif($this->getReference(TarifFixtures::SODA_TARIF_REFERENCE));
        $manager->persist($orangina);

        $bzhCola = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::SOFT_CATEGORIE_REFERENCE))
            ->setNom('Breizh Cola')
            ->setGaz(true)
            ->setType($this->getReference(TypeFixtures::SODA_TYPE_REFERENCE))
            ->setPichet(true)
            ->setStock(300)
            ->setTarif($this->getReference(TarifFixtures::SODA_TARIF_REFERENCE));
        $manager->persist($bzhCola);

        $bzhTea = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::SOFT_CATEGORIE_REFERENCE))
            ->setNom('Breizh Tea')
            ->setGaz(false)
            ->setType($this->getReference(TypeFixtures::ICETEA_TYPE_REFERENCE))
            ->setPichet(true)
            ->setStock(300)
            ->setTarif($this->getReference(TarifFixtures::SODA_TARIF_REFERENCE));
        $manager->persist($bzhTea);

        $eau = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::SOFT_CATEGORIE_REFERENCE))
            ->setNom('Eau de source')
            ->setGaz(false)
            ->setType($this->getReference(TypeFixtures::EAU_TYPE_REFERENCE))
            ->setPichet(true)
            ->setStock(300)
            ->setTarif($this->getReference(TarifFixtures::EAU_TARIF_REFERENCE));
        $manager->persist($eau);

        $perrier = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::SOFT_CATEGORIE_REFERENCE))
            ->setNom('Perrier')
            ->setGaz(false)
            ->setType($this->getReference(TypeFixtures::EAU_TYPE_REFERENCE))
            ->setPichet(true)
            ->setStock(200)
            ->setTarif($this->getReference(TarifFixtures::SODA_TARIF_REFERENCE));
        $manager->persist($perrier);

        $hk = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::BEER_CATEGORIE_REFERENCE))
            ->setNom('Heineken')
            ->setAlcool(5)
            ->setStyle($this->getReference(StyleFixtures::PILS_STYLE_REFERENCE))
            ->setPichet(true)
            ->setStock(300)
            ->setTarif($this->getReference(TarifFixtures::BIERE_BASE_TARIF_REFERENCE));
        $manager->persist($hk);

        $debaucheIPA = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::BEER_CATEGORIE_REFERENCE))
            ->setNom('IPA - La Débauche')
            ->setAlcool(6)
            ->setStyle($this->getReference(StyleFixtures::IPA_STYLE_REFERENCE))
            ->setPichet(false)
            ->setStock(200)
            ->setTarif($this->getReference(TarifFixtures::CRAFT_BEER_TARIF_REFERENCE));
        $manager->persist($debaucheIPA);

        $hefe = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::BEER_CATEGORIE_REFERENCE))
            ->setNom('Weihenstephaner Hefe Weissbier')
            ->setAlcool(5.4)
            ->setStyle($this->getReference(StyleFixtures::WEIZEN_STYLE_REFERENCE))
            ->setPichet(true)
            ->setStock(200)
            ->setTarif($this->getReference(TarifFixtures::BIERE_BASE_TARIF_REFERENCE));
        $manager->persist($hefe);

        $eroica = (new Produit())
            ->setCategorie($this->getReference(CategorieFixtures::BEER_CATEGORIE_REFERENCE))
            ->setNom('Eroica - Piggy Brewing Company')
            ->setAlcool(6)
            ->setStyle($this->getReference(StyleFixtures::NEIPA_STYLE_REFERENCE))
            ->setPichet(false)
            ->setStock(150)
            ->setTarif($this->getReference(TarifFixtures::CRAFT_BEER_TARIF_REFERENCE));
        $manager->persist($eroica);


        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['type'];
    }

    public function getOrder(): int
    {
        return 5;
    }

}
