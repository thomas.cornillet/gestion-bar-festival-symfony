<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/produit', name: 'app_produit_')]
class ProduitController extends AbstractController
{
    #[Route('/catalogue', name: 'catalogue', methods: 'GET')]
    public function index(): Response
    {
        return $this->render('produit/catalogue.html.twig', []);
    }
}
